import React from 'react';
import './App.css';

const { Component } = React

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentView: '',
      ConnectData: '',
      UserCreateName: '',
      CreateUserValidation: '',
      UserCreateEmail: '',
      CreateEmailValidation: '',
      UserCreatePass: '',
      CreatePasswordValidation: '',
      UserCreatePhone: '',
      CreatePhonevalidation: '',
      loginUserDiv: '',
      PrintDataUser: [],
      changeSectionView: '',
      createSubmit:'',
    }
  }
  TotalData = () => {
    fetch('http://localhost:4000/user', {
      headers: { 'Content-type': 'application/json' },
      method: 'GET'
    }).then(response => response.json()).then(data => 
      console.log("total database received")
      )
  }
  componentDidMount() {
    this.TotalData();
    let internetConnection = window.navigator.onLine;
    this.setState({ currentView: 'Connecting', CreateEmailValidation:''})
    fetch('http://localhost:4000/user/connect', {
      headers: { 'Content-type': 'application/json' },
      method: 'GET'
    })
      .then(response => response.json())
      .then(resData => { this.setState({ ConnectData: resData }) })
      .catch((error) => { console.log(error) });
    console.log(this.state.ConnectData);
    setTimeout(() => {
      if (internetConnection === true && this.state.ConnectData === "Connect") {
        this.setState({ currentView: "logIn" })
      } else if (this.state.ConnectData === "" && internetConnection === true) {
        this.setState({ currentView: "ServerError" })
      }
      else { this.setState({ currentView: "NoInternet" }) }
    }, 1000)
  }
  changeView = (view) => {
    this.setState({ currentView: view, UserLoginPassword: '' ,CreateEmailValidation:''})
  } // component toggle view
  UserCreateName = (e) => {
    if (/^[A-Za-z]{6,15}$/.test(String(e.target.value))) {
      this.setState({ UserCreateName: e.target.value, CreateUserValidation: "Valid Username" })
      setTimeout(() => {
        this.searchNameFunction()
      }, 2000)
    } else { this.setState({ CreateUserValidation: "Enter correct username format" }) }
  } //user create name
  searchNameFunction = () => {
    var data = JSON.stringify({
      "name": this.state.UserCreateName
    });
    fetch('http://localhost:4000/user/findname', {
      body: data,
      headers: { 'Content-type': 'application/json' },
      method: 'POST'
    }).then(response => response.json())
      .then(data => this.setState({ CreateUserValidation: data }))
    setTimeout(() => {
      if (this.state.CreateUserValidation === "Username already exists" || this.state.CreateUserValidation === "Enter correct username format") {
        this.setState({ UserCreateName: '' })
      } else { }
    }, 1000)
  }
  UserCreateEmail = (e) => {
    if (/^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/.test(e.target.value) && e.target.value.length >= 7) {
      this.setState({ UserCreateEmail: e.target.value, CreateEmailValidation: "email address is valid" })
      setTimeout(()=> {
        this.searchEmailFunction();
      },1000)
    } else {
      this.setState({ CreateEmailValidation: "enter valid email address" })
    }
  } // user create email
  searchEmailFunction = () => {
    var data = JSON.stringify({
      "email": this.state.UserCreateEmail
    });
    fetch('http://localhost:4000/user/findemail', {
      body: data,
      headers: { 'Content-type': 'application/json' },
      method: 'POST'
    }).then(response => response.json())
      .then(data => this.setState({ CreateEmailValidation: data }))
    setTimeout(() => {
      if (this.state.CreateEmailValidation === "Email already exists" || this.state.CreateEmailValidation === "enter valid email address") {
        this.setState({ UserCreateEmail: '' })
      } else { }
    }, 1000)
  }
  UserCreatePhone = (e) => {
    if (/^[0]?[789]\d{9}$/.test(e.target.value)) { 
      this.setState({ CreatePhonevalidation: "Phone number is valid", UserCreatePhone: e.target.value }) 
      setTimeout(()=> {
        this.searchPhoneFunction();
      },1000)
    } else { this.setState({ CreatePhonevalidation: "enter valid phone number" }) }
  } // user create phone
  searchPhoneFunction = () => {
    var data = JSON.stringify({
      "phone": this.state.UserCreatePhone
    });
    fetch('http://localhost:4000/user/findphone', {
      body: data,
      headers: { 'Content-type': 'application/json' },
      method: 'POST'
    }).then(response => response.json())
      .then(data => this.setState({ CreatePhonevalidation: data }))
    setTimeout(() => {
      if (this.state.CreatePhonevalidation === "Phone number already exists" || this.state.CreatePhonevalidation === "enter valid phone number") {
        this.setState({ UserCreatePhone: '' })
      } else { }
    }, 1000)
  }
  userCreatePassword = (e) => {
    if (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/.test(e.target.value)) {
      this.setState({ CreatePasswordValidation: "valid password", UserCreatePass: e.target.value })
    } else { this.setState({ CreatePasswordValidation: "Min 8 digits, one num ,one uppercase letter,one special char" }) }
  } //user create password
  submitFormData = (e) => {
    var resetForm = document.getElementById('Signup');
    e.preventDefault();
    if (this.state.CreateUserValidation === "Valid Username" && this.state.CreateEmailValidation === "email address is valid" && this.state.CreatePhonevalidation === "Phone number is valid" && this.state.CreatePasswordValidation === "valid password") {
      this.setState({currentView:"Connecting", createSubmit:false})
      setTimeout(() => {
        this.setState({currentView:"SuccessMsg"})
      }, 900);
      var data = JSON.stringify({
        "id": "",
        "name": this.state.UserCreateName,
        "email": this.state.UserCreateEmail,
        "phone": Number(this.state.UserCreatePhone),
        "password": this.state.UserCreatePass
      });
      fetch('http://localhost:4000/user/login', {
        body: data,
        headers: { 'Content-type': 'application/json' },
        method: 'POST'
      })
        .then(response => response.json())
        .then(resData => { console.log(resData); })
        .catch((error) => { console.log(error) });
      setTimeout(() => {
        resetForm.reset();
        this.setState({ CreateUserValidation: '', CreateEmailValidation: '', CreatePhonevalidation: '', CreatePasswordValidation: '', currentView: "logIn" })
      }, 2500)
      this.TotalData();
    }
    else { alert("Please enter all fields correctly"); this.setState({createSubmit:true}) }
  } //user create submit
  signinCheck = (event) => {
    event.preventDefault();
    if (this.state.UserLoginEmail !== "" && this.state.UserLoginPassword !== "") {
      var data = JSON.stringify({
        "email": this.state.UserLoginEmail,
        "password": this.state.UserLoginPassword
      });
      fetch('http://localhost:4000/user/loginEntry', {
        body: data,
        headers: { 'Content-type': 'application/json' },
        method: 'POST'
      }).then(response => response.json())
        .then(data => this.setState({ loginUserDiv: data }))
        this.closeNav();
        this.changeViewData();
    } else {
      alert("Please enter all fields")
    }
  } //user sign in 
  changeViewData = () => {
    this.setState({ currentView: "Connecting" })
    setTimeout(()=> {
      if (this.state.loginUserDiv === "email and password match successfully") {
          // this.setState({ currentView: "Connecting" })
          this.searchDataLoad();
        setTimeout(() => {
          this.setState({ currentView: "" })
        }, 1000)
      } else {
          this.setState({currentView:"LoginError"})
          console.log("email or password is incorrect");
      }
    },1000)
  }
  searchDataLoad = () => {
    // /user/SendData
    var data = JSON.stringify({
      "email": this.state.UserLoginEmail,
      "password": this.state.UserLoginPassword
    });
    fetch('http://localhost:4000/user/SendData', {
      body: data,
      headers: { 'Content-type': 'application/json' },
      method: 'POST'
    }).then(response => response.json())
      .then(data => this.setState({ PrintDataUser: data ,changeSectionView:"Gallery"}))
  }
  UserLoginEmail = (e) => { 
    if (/^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/.test(e.target.value) && e.target.value.length >= 7) {
      this.setState({ UserCreateEmail: e.target.value, CreateEmailValidation: "email address is valid" })
      this.setState({ UserLoginEmail: e.target.value, UserCreateEmail:e.target.value })
      setTimeout(()=> {
        this.searchEmailFunction();
      },1000) 
    } else {
      this.setState({CreateEmailValidation:'', UserLoginEmail:''}) 
    }
   } // userlogin name 
  UserLoginPassword = (e) => { this.setState({ UserLoginPassword: e.target.value }) }  //userlogin password
  HaveAccount = () => {
    this.setState({ currentView: "logIn", CreateEmailValidation: '', CreatePhonevalidation: '', CreatePasswordValidation: '', UserCreateName: '', UserCreateEmail: '', UserCreatePhone: '', UserCreatePass: '', CreateUserValidation: '' })
  }
  logoutEntry = () => {
    setTimeout( ()=> {
      this.setState({currentView:"Connecting",CreateEmailValidation:''})
    },1000)
    setTimeout(() => {
      this.setState({ currentView: "logIn", UserLoginEmail: '', UserLoginPassword: '' })
    }, 2000)
  }
  openNav = () => {
    document.getElementById("mySidenav").style.width = "250px";
  }
  closeNav = () => {
    document.getElementById("mySidenav").style.width = "0";
  }
  OpenGallery = () => {
    document.getElementsByClassName('WelcomeDiv').style.display = 'none';
    document.getElementsByClassName('GalleryDiv').style.display = 'block';
  }
  changeSectionView = () => {
    switch (this.state.changeSectionView) {
      case "Gallery":
        return (
          <div className="bodyDivContent">Gallery</div>
        )
      case "Upload":
        return (
          <div className="bodyDivContent">Videos</div>
        )
      case "Download":
        return (
          <div className="bodyDivContent">Files</div>
        )
      case "Contact":
        return (
          <div>
            <p className="bodyDivContent">Personal</p>
            <p>Name:<span className="settingsDataSpan">{this.state.PrintDataUser[0]}</span></p>
            <p>ID:<span className="settingsDataSpan">{this.state.PrintDataUser[1]}</span></p>
            <p>Email:<span className="settingsDataSpan">{this.state.PrintDataUser[2]}</span></p>
            <p>Phone:<span className="settingsDataSpan">{this.state.PrintDataUser[3]}</span></p>
            <p>Reg Date:<span className="settingsDataSpan">{this.state.PrintDataUser[4]}</span></p>
            <p>PDF:<a href = {this.state.PrintDataUser[5]}>click</a></p>
            <p></p>
          </div>
        )
        case "Settings" :
          return (
           <div className="bodyDivContent">Settings</div>
          )
        default:
          break
    }
  }
  changeViewDiv = (view) => {
    // this.setState({ })
    // console.log("View Data",view);
    setTimeout(()=>{
      document.getElementById("mySidenav").style.width = "0";
    },200)
    this.setState({changeSectionView:view})
  } // component toggle view
  currentView = () => {
    let toggleShownameMsg = this.state.CreateUserValidation === "Enter correct username format" || this.state.CreateUserValidation === "Username already exists" ? "ShowErrorMsg" : "ShowCorrectMsg";
    let toggleShowemailMsg = this.state.CreateEmailValidation === "enter valid email address" || this.state.CreateEmailValidation === "Email already exists" ? "ShowErrorMsg" : "ShowCorrectMsg";
    let toggleShowemailMsgFind = this.state.CreateEmailValidation === "enter valid email address" || this.state.CreateEmailValidation === "Email already exists" ? "ShowCorrectMsg" : "ShowErrorMsg";
    let toggleShowPhoneMsg = this.state.CreatePhonevalidation === "enter valid phone number" || this.state.CreatePhonevalidation === "Phone number already exists" ? "ShowErrorMsg" : "ShowCorrectMsg";
    let toggleShowPassMsg = this.state.CreatePasswordValidation === "valid password" ? "ShowCorrectMsg" : "ShowErrorMsg";
    switch (this.state.currentView) {
      case "signUp":
        return (
          <form id="Signup">
            <p className="usershowdata">{this.state.userShowDetails}</p>
            <h2>Sign Up!</h2>
            <fieldset>
              <legend>Create Account</legend>
              <ul>
                <li>
                  <label>Username:<span className={toggleShownameMsg}>{this.state.CreateUserValidation}</span></label>
                  <input type="text" id="sign-username" required placeholder="Username" onChange={this.UserCreateName} />
                </li>
                <li>
                  <label>Email:<span className={toggleShowemailMsg}>{this.state.CreateEmailValidation}</span></label>
                  <input type="email" id="sign-email" required placeholder="Email" onChange={this.UserCreateEmail} />
                </li>
                <li>
                  <label>Mobile:<span className={toggleShowPhoneMsg}>{this.state.CreatePhonevalidation}</span></label>
                  <input type="tel" id="phone-number" required placeholder="Mobile No" onChange={this.UserCreatePhone} />
                </li>
                <li>
                  <label>Password:<span className={toggleShowPassMsg}>{this.state.CreatePasswordValidation}</span></label>
                  <input type="password" id="sign-password" required placeholder="Password" onChange={this.userCreatePassword} />
                </li>
              </ul>
            </fieldset>
            <button onClick={this.submitFormData} disabled={this.state.createSubmit}>Submit</button>
            <button type="button" onClick={this.HaveAccount}>Have an Account?</button>
          </form>
        )
      case "logIn":
        return (
          <form>
            <h2>Welcome Back!</h2>
            <fieldset>
              <legend>Log In</legend>
              <ul>
                <li>
                  <label>Email:{this.state.CreateEmailValidation.length === 0 ? '' :
                    <span className={toggleShowemailMsgFind}>{this.state.CreateEmailValidation !== "Email already exists" ? "email not registered" : "email address valid"}</span>}
                    </label>
                  <input type="email" id="login-username" required placeholder="Email" onChange={this.UserLoginEmail} />
                </li>
                <li>
                  <label>Password:</label>
                  <input type="password" id="login-password" required placeholder="Password" onChange={this.UserLoginPassword} disabled={this.state.UpdatePasswordEntry} />
                </li>
                <li>
                  <i />
                  <p className="passwordReset" onClick={() => this.changeView("PWReset")}>Forgot Password?</p>
                  {/* <div class="loader"></div> */}
                </li>
              </ul>
            </fieldset>
            <button onClick={this.signinCheck}>Login</button>
            <button type="button" onClick={() => this.changeView("signUp")}>Create an Account</button>
          </form>
        )
      case "PWReset":
        return (
          <form>
            <h2>Reset Password</h2>
            <fieldset>
              <legend>Password Reset</legend>
              <ul>
                <li>
                  <em>A reset OTP will be sent to your Mobile Number!</em>
                </li>
                <li>
                  <label>Email:</label>
                  <input type="email" id="reset-email" required placeholder="Email" />
                </li>
                <li>
                  <label>Mobile:</label>
                  <input type="Mobile" id="reset-password" required placeholder="Mobile" />
                </li>
              </ul>
            </fieldset>
            <button>Send Reset Link</button>
            <button type="button" onClick={() => this.changeView("logIn")}>Go Back</button>
          </form>
        )
      case "mainpage":
        return (
          <div className="MainPageDiv">main page</div>
        )
      case "Connecting":
        return (
          <form>
            <fieldset>
              <legend></legend>
              <ul>
                <p className="loadingDiv">Loading...</p>
              </ul>
            </fieldset>
          </form>
        )
        case "LoginError":
        return (
          <form>
            <fieldset>
              <legend></legend>
              <ul>
                <p className="loadingDiv">password is incorrect</p>
                <button type="button" onClick={() => this.changeView("logIn")}><i className="fa fa-arrow-circle-left"></i>  Go Back</button>
                <button type="button" onClick={() => this.changeView("PWReset")} className="resetPassword">Reset Passowrd  <i className="fa fa-arrow-circle-right"></i></button>
              </ul>
            </fieldset>
          </form>
        )
        // SuccessMsg
        case "SuccessMsg":
        return (
          <form>
            <fieldset>
              <legend></legend>
              <ul>
                <p className="loadingDiv">User Added Successfully :) <p>Please Login again</p></p>
              </ul>
            </fieldset>
          </form>
        )
      case "ServerError":
        return (
          <form>
            <fieldset>
              <legend></legend>
              <ul>
                <p className="loadingDiv">Server Error</p>
              </ul>
            </fieldset>
          </form>
        )
      case "NoInternet":
        return (
          <form>
            <fieldset>
              <legend></legend>
              <ul>
                <p className="loadingDiv">No Internet :(</p>
              </ul>
            </fieldset>
          </form>
        )
      default:
        break
    }
  }
  render() {
    let divChange = this.state.currentView === "" ? "showDiv" : "emptyDiv";
    let changeSection = this.state.currentView !== "" ? "AddId" : "RemoveId"
    return (
      <div>
        <section id={changeSection}>{this.currentView()}</section>
        <div className={divChange}>
          <div id="mySidenav" className="sidenav">
            <div className="closebtn">
              <span className="spanHeadIcons" title="Notifications"><i className="fa fa-bell"></i></span>
              <span onClick={this.logoutEntry} className="spanHeadIcons" title="Log out"><i className="fa fa-power-off"></i></span>
              <span onClick={this.closeNav} className="spanHeadIcons" title="close"><i className="fa fa-close"></i></span>
            </div>
            <p onClick={() => this.changeViewDiv("Gallery")}>Gallery</p>
            <p onClick={() => this.changeViewDiv("Upload")}>Videos</p>
            <p onClick={() => this.changeViewDiv("Download")}>Files</p>
            <p onClick={() => this.changeViewDiv("Contact")}>Personal</p>
            <p onClick={() => this.changeViewDiv("Settings")}>Settings</p>
          </div>
          <span onClick={this.openNav} className="Openbtn" title="open"><i className="fa fa-navicon"></i> <span className="displayName">Hello! {this.state.PrintDataUser[0]}</span></span>
         {this.state.PrintDataUser.length === 0 ? '' :
            <div className="bodyContentDiv">
              <div className="GalleryDiv">
                {/* <p style={{textAlign:"right"}}>Hello! {this.state.PrintDataUser[0]}</p> */}
                <section id="bodyDivChange">{this.changeSectionView()}</section>
              </div>
            </div>
          }
        </div>
      </div>
    )
  }
}
export default App;